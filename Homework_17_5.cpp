#include <iostream>
#include <cmath>

class MyClass
{
private:
	std::string text = "Hello from MyClass!";

public:
	void SetText(std::string newText)
	{
		text = newText;
	}
	void GetText()
	{
		std::cout << text << '\n';
	}
};

class Vector
{
private:
	double x = 0;
	double y = 0;
	double z = 0;

public:
	Vector() : x(5), y(5), z(5)
	{}
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}
	void Show()
	{
		std::cout << '\n' << x << ' ' << y << ' ' << z;
	}
	void Module()
	{
		// ����� hypot �� ����� ��������� 3 ���������
		double mod = sqrt(pow(x,2) + pow(y, 2) + pow(z, 2));
		std::cout << '\n' << "module of v: " << mod;
	}
};

int main()
{
	MyClass mc;
	mc.SetText("Hello from mc");
	mc.GetText();

	Vector v;
	v.Show();
	v.Module();
}